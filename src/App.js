import React, { useCallback, useState } from 'react';
import MultilineEllipsis from './components/MultilineEllipsis';
import ProgressBar from './components/ProgressBar';
import InputRange from './components/InputRange';
import styles from './App.module.scss';

const progressMaxValue = 500;

function App() {
  const [state, setState] = useState({
    text: '',
    parentHeight: 20,
    progress: 0,
  });

  const handleChange = useCallback((e) => {
    const {name, value} = e.target;

    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  }, []);

  const handleRemoveText = () => {
    setState((prev) => ({
      ...prev,
      text: '',
    }));
  };

  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <div className={styles.column}>
          <h2>Компонент "Прогресс-бар с лодкой".</h2>

          <div className={styles.actions}>
             <InputRange
              name="progress"
              label={`Значение прогресса: ${state.progress}`}
              value={state.progress}
              onChange={handleChange}
              max={500}
             />
          </div>
          
          <div className={styles.parentForProgress}>
            <ProgressBar currentValue={+state.progress} maxValue={progressMaxValue} />
          </div>
        </div>
        
        <div className={styles.column}>
          <h2>Компонент "Multiline ellipsis".</h2>

          <textarea
            name='text'
            rows={6}
            placeholder='Введите текст'
            className={styles.textarea}
            value={state.text}
            onChange={handleChange}
          />

          <div className={styles.actions}>
            <InputRange
              name="parentHeight"
              label={`Высота родительского блока: ${state.parentHeight}px`}
              value={+state.parentHeight}
              onChange={handleChange}
              step={20}
              min={20}
              max={360}
             />
            
            <button onClick={handleRemoveText}>Очистить</button>
          </div>

          <div className={styles.parentForEllipsis} style={{ height: `${state.parentHeight}px` }}>
            <MultilineEllipsis
              text={state.text}
              parentHeight={+state.parentHeight}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
