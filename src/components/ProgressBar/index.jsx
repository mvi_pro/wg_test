import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Boat from './Boat';
import styles from './style.module.scss';

const boatSize = {
  width: 64,
  height: 44,
}

const ProgressBar = memo(({
  currentValue,
  maxValue,
}) => {
  const value = (currentValue / maxValue) * 100;

  return (
    <div className={styles.progressBar}>
      <div
        className={styles.boat}
        style={{ left: `calc(${value}% - ${boatSize.width / 2}px)` }}
      >
        <span className={styles.progressValue}>
          {currentValue === 0 ? currentValue : currentValue.toFixed(3)}
        </span>
        <Boat
          color='#ffa62d'
          {...boatSize}
        />
      </div>
      
      <div className={styles.wave} />
      <div className={styles.bar}>
        <div className={styles.line} />
        <div
          className={styles.progress}
          style={{ width: `${value}%` }}
        />
      </div>
    </div>
  );
});

ProgressBar.propTypes = {
  currentValue: PropTypes.number.isRequired,
  maxValue: PropTypes.number.isRequired,
}

export default ProgressBar;