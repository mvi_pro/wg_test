import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';

const InputRange = memo(({
  name,
  value,
  label,
  step,
  min,
  max,
  onChange,
}) => {
  return (
    <div className={styles.inputRange}>
      {label && <label htmlFor={name}>{label}</label>}
      <input
        id={name}
        name={name}
        type="range"
        step={step}
        min={min}
        max={max}
        value={value}
        onChange={onChange}
      />
    </div>
  );
});

InputRange.defaultProps = {
  label: '',
  step: 1,
  min: 0,
  max: 100,
}

InputRange.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  label: PropTypes.string,
  step: PropTypes.number,
  min: PropTypes.number,
  max: PropTypes.number,
  onChange: PropTypes.func.isRequired,
}

export default InputRange;