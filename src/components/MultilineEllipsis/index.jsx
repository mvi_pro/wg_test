import React, { useRef, useEffect, useState, memo } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './style.module.scss';

const MultilineEllipsis = memo(({
  text,
  tag,
  className,
  lineHeight,
  fontSize,
  parentHeight,
}) => {
  const textRef = useRef(null);

  const [overflow, setOverflow] = useState(false);
  const [lineClamp, setLineClamp] = useState(0);

  useEffect(() => {
    if (textRef?.current) {
      const element = textRef.current;

      if (element.parentElement.clientHeight < element.clientHeight) {
        setOverflow(true);
      }
      setLineClamp(+(element.parentElement.clientHeight / lineHeight / fontSize).toFixed())
    }
  }, [
    text,
    lineHeight,
    fontSize,
    parentHeight,
  ]);

  const Tag = tag;

  if (!text) {
    return null;
  }

  return (
    <Tag
      ref={textRef}
      style={{
        WebkitLineClamp: lineClamp,
        lineHeight: +lineHeight,
        fontSize: `${fontSize}px`,
      }}
      className={cn(
        styles.multilineEllipsis,
        {[styles.overflow]: overflow},
        className,
      )}
    >
      {text}
    </Tag>
  );
});

MultilineEllipsis.defaultProps = {
  tag: 'p',
  className: '',
  lineHeight: 1.4,
  fontSize: 14,
  parentHeight: 0,
}

MultilineEllipsis.propTypes = {
  text: PropTypes.string.isRequired,
  tag: PropTypes.string,
  className: PropTypes.string,
  lineHeight: PropTypes.number,
  fontSize: PropTypes.number,
  parentHeight: PropTypes.number,
}

export default MultilineEllipsis;
